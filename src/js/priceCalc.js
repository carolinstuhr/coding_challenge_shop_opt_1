const inputField = document.querySelector('.input-field')
window.onload = function () {
  inputField.value = '1'
}

const priceElement = document.querySelector('.price-result')
priceElement.innerHTML = `<span>${Math.round(19.95 * 0.9 * 100) / 100} €</span>`

inputField.addEventListener('input', calculatePrice)

function calculatePrice(event) {
  let amount = event.target.value
  let price
  if (amount < 3) {
    price = amount * 19.95 * 0.9
  } else if (amount < 5) {
    price = amount * 19.95 * 0.9 * 0.8
  } else {
    price = amount * 19.95 * 0.9 * 0.7
  }
  priceElement.innerHTML = `<span>${Math.round(price * 100) / 100} €</span>`
}
